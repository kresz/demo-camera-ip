import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { SocketWebService } from '../socket-web.service';
import JSMpeg from '@cycjimmy/jsmpeg-player';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  //video: HTMLVideoElement;
  //@ViewChild('video', { static: false }) video: HTMLVideoElement;


  public source: any;
  socketService: SocketWebService = new SocketWebService();

  //@Input() stream: any;
  // @ViewChild('streaming', {static: false}) streamingcanvas: ElementRef; 

 // @Input() stream: any;
 // @ViewChild('streaming', {static: false}) streamingcanvas: ElementRef; 

 // @ViewChild('streaming', {static: false}) streamingcanvas: ElementRef; 

 //@ViewChild('videoPlayer') videoplayer: ElementRef;

  constructor() {
 

    

    /*
    this.video = document.createElement('video');
    const myMediaSource = new MediaSource();
    let videoSourceBuffer;
    myMediaSource.addEventListener('sourceopen', () => {
      videoSourceBuffer = myMediaSource.addSourceBuffer('video/mp4; codecs="avc1.64001e"')
    });
    // const videoSourceBuffer = myMediaSource.addSourceBuffer('video/mp4; codecs="avc1.64001e"')
    const url = URL.createObjectURL(myMediaSource);
    this.video.src = url;


    var video1 = document.querySelector("#video1");
    video1.src = URL.createObjectURL(myMediaSource)

    socketService.outEven.subscribe(res => {
      sourceBuffer.appendBuffer(d);
      // const blob = new Blob([res]);
      //this.source = URL.createObjectURL(blob);
      console.log('data--> ', res);
      // const videoData = res.arrayBuffer()

      //videoSourceBuffer.appendBuffer(videoData)

    });*/
   }

  ngOnInit(): void {

  }



  ngAfterContentInit() {

  }

  ngAfterViewInit() {

    let video = document.querySelector("#video1");
    console.log("#video1");
    console.log(video);
    let mediaSource = new MediaSource();
    let sourceBuffer;
    mediaSource.addEventListener("sourceopen", function()
    {
      sourceBuffer = mediaSource.addSourceBuffer("video/webm;codecs=vp8,opus");
    });

    if(video != null) {
      (video as HTMLVideoElement).src = URL.createObjectURL(mediaSource)
    }
    

    this.socketService.outEven.subscribe(res => { 
      console.log(res);
      console.log(mediaSource.readyState);
      if(mediaSource.readyState == 'open') {
        sourceBuffer.appendBuffer(res);
      }

      

    });




    /*let player = new JSMpeg.Player('ws://localhost:9999', {
      canvas: this.streamingcanvas.nativeElement, autoplay: true, audio: false, loop: true
    })*/
  }
}


/*
import { Component, OnInit } from '@angular/core';
import { SocketWebService } from '../socket-web.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public video: any;

  constructor(protected socketService: SocketWebService) {
    socketService.outEven.subscribe(res => {
      this.video = 'data:image/jpeg;base64,' + res;
      console.log('video');
      console.log(this.video);
    });
   }

  ngOnInit(): void {
  }

}
*/
    

    /*let player = new JSMpeg.Player('ws://localhost:9999', {
      canvas: this.streamingcanvas, autoplay: true, audio: false, loop: true
    })
*/
    

/*
    socketService.outEven.subscribe(res => {
      this.video = res;
      console.log('video');
      console.log(this.video);
    });
   }
/*
  ngOnInit(): void {
  }

}*/
