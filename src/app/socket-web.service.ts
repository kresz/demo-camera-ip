import { EventEmitter, Injectable, Output } from '@angular/core';
import { Socket } from 'ngx-socket-io';


@Injectable({
  providedIn: 'root'
})
export class SocketWebService extends Socket{

  @Output() outEven: EventEmitter<any> = new EventEmitter(); 

  constructor() {
    super({
      url: 'http://localhost:4000',
    });


    this.ioSocket.on('video-stream', (res: any) => this.outEven.emit(res))
   }
}
